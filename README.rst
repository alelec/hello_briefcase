===============
Hello Briefcase
===============
This simple project is used as a testbed for briefcase project: https://github.com/pybee/briefcase

Gitlab CI is used to build this project as a distributable application on a wide range of platforms.

See `.gitlab-ci.yaml` for details.
