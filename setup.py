import os
from setuptools import setup


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    long_description = readme.read()


setup(
    name='hello_briefcase',
    py_modules=['hello_briefcase'],
    include_package_data=True,
    description='Basic test application for briefcase',
    long_description=long_description,
    author='Andrew Leech',
    author_email='andrew@alelec.net',
    url='https://gitlab.alelec.net/alelec/hello_briefcase',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    install_requires=['setuptools_scm', 'briefcase'],
    entry_points={
        'console_scripts': [
            'hello_briefcase = hello_briefcase:main',
        ],
    },
    options={
        'app': {
            'formal_name': 'hello_briefcase'
        }
    }
)